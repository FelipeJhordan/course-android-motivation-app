package felipe.pdm.kotlin_course_motivation_app.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import felipe.pdm.kotlin_course_motivation_app.R
import felipe.pdm.kotlin_course_motivation_app.databinding.ActivitySplashBinding
import felipe.pdm.kotlin_course_motivation_app.infra.MotivationConstants
import felipe.pdm.kotlin_course_motivation_app.infra.SecurityPreferences

class SplashActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var mSecurityPreferences :SecurityPreferences
    private lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)
        mSecurityPreferences = SecurityPreferences(this)

        if( supportActionBar != null) {
            supportActionBar!!.hide();
        }

        // I Usually like do with function referenced at the layout xml
        binding.btnSalvar.setOnClickListener(this)

        val securityPreferences = SecurityPreferences(this)

        verifyName()
    }

     override fun onClick( view : View) {
        val id = view.id
        if( id == R.id.btnSalvar) {
            handleSave()
        }
    }

    private fun handleSave() {
        val name = binding.edNome.text.toString()
        if( name != "") {
            mSecurityPreferences.storeString(MotivationConstants.KEY.PERSON_NAME, name)
            navigateToMain()
        } else {
            Toast.makeText(this,"Informe seu nome!" ,Toast.LENGTH_SHORT)
        }
    }

    private fun verifyName() {
       val name =  mSecurityPreferences.getString(MotivationConstants.KEY.PERSON_NAME)
        if ( name != ""  ) {
            navigateToMain()
        }
    }

    private fun navigateToMain() {
        val intent = Intent( this, MainActivity::class.java)
        startActivity(intent)
        // eliminar activity da memória
        finish()
    }
}