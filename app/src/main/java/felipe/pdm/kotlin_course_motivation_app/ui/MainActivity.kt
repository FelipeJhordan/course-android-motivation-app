package felipe.pdm.kotlin_course_motivation_app.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import felipe.pdm.kotlin_course_motivation_app.R
import felipe.pdm.kotlin_course_motivation_app.databinding.ActivityMainBinding
import felipe.pdm.kotlin_course_motivation_app.infra.MotivationConstants
import felipe.pdm.kotlin_course_motivation_app.infra.SecurityPreferences
import felipe.pdm.kotlin_course_motivation_app.repository.Mock

open class MainActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var mSecurityPreferences: SecurityPreferences
    private lateinit var binding: ActivityMainBinding

    private var mPhraseFilter: Int = MotivationConstants.PHRASEFILTER.ALL
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        mSecurityPreferences = SecurityPreferences(this)
        val name = mSecurityPreferences.getString(MotivationConstants.KEY.PERSON_NAME).toString()
        binding.txtName.text = "Olá, $name!"

        if( supportActionBar != null) {
            supportActionBar!!.hide()
        }

        binding.ivAll.setColorFilter(resources.getColor(R.color.colorAccent, null))
        handleNewPhrase()

        binding.btnNewPhrase.setOnClickListener(this)
        binding.ivAll.setOnClickListener(this)
        binding.ivHappy.setOnClickListener(this)
        binding.ivSunny.setOnClickListener(this)
    }

    override fun onClick(view: View) {

        val id = view.id
        val listFilter = listOf(R.id.iv_all, R.id.iv_happy, R.id.iv_sunny)
        if (id == R.id.btnNewPhrase) {
            handleNewPhrase()
        } else if (id in listFilter) {
            handleFilter(id)
        }
    }

    private fun handleNewPhrase() {
        val newText = Mock().getPhrase(mPhraseFilter)
        if(  newText != binding.txtPhrase.text) {
            binding.txtPhrase.text = newText
        } else {
            handleNewPhrase()
        }
    }

    private fun handleFilter(id: Int) {
        changeColorToRoot()
        when (id) {
            R.id.iv_all -> {
                binding.ivAll.setColorFilter(resources.getColor(R.color.colorAccent, null))
                mPhraseFilter = MotivationConstants.PHRASEFILTER.ALL
            }
            R.id.iv_happy -> {
                binding.ivHappy.setColorFilter(resources.getColor(R.color.colorAccent, null))
                mPhraseFilter = MotivationConstants.PHRASEFILTER.HAPPY
            }
            R.id.iv_sunny -> {
                binding.ivSunny.setColorFilter(resources.getColor(R.color.colorAccent, null))
                mPhraseFilter = MotivationConstants.PHRASEFILTER.MORNING
            }
        }
    }

    private fun changeColorToRoot() {
        binding.ivSunny.setColorFilter(resources.getColor(R.color.white, null))
        binding.ivHappy.setColorFilter(resources.getColor(R.color.white, null))
        binding.ivAll.setColorFilter(resources.getColor(R.color.white, null))
    }

}